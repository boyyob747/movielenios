//
//  MovieApi.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import Foundation
import Alamofire

class MovieApi {
    let BASE_URL = "http://127.0.0.1:3000/"
}
extension MovieApi{
    
    func login(username:String,password:String,onSuccess success: @escaping (_ result: Result) -> Void){
        let parameters:[String:Any] = [
            "username" : username,
            "password" : password
            ]
        let todoEndpoint: String = BASE_URL + "login"
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        Alamofire.request(todoEndpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            if let res = response.result.value as? [String:Any]{
                let result = Result.init(fromDictionary: res)
                success(result)
            }
        }
    }
    func getUser(username:String,onSuccess success: @escaping (_ user: User?) -> Void){
        let todoEndpoint: String = BASE_URL + "get_user/" + username
        Alamofire.request(todoEndpoint, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            if let res = response.result.value as? [String:Any]{
                let user = User.init(fromDictionary: res)
                success(user)
            }else{
                success(nil)
            }
        }
    }
    
    func register(username:String,password:String,onSuccess success: @escaping (_ result: Result) -> Void){
        let parameters:[String:Any] = [
            "username" : username,
            "password" : password
        ]
        let todoEndpoint: String = BASE_URL + "register"
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        Alamofire.request(todoEndpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            if let res = response.result.value as? [String:Any]{
                let result = Result.init(fromDictionary: res)
                success(result)
            }
        }
    }
}
extension MovieApi{
    func getTopMovieRatings(){
        Alamofire.request(BASE_URL + "get_top_ratings").responseJSON { (results) in
            if let resultJson = results.result.value as? [String:Any]{
                print("DebugApi",resultJson)
            }
        }
    }
    func getRecommender(userId:Int,onSuccess success: @escaping (_ movies: [Movie]) -> Void){
        Alamofire.request(BASE_URL + "get_recommender/\(userId)").responseJSON { (results) in
            var movies = [Movie]()
            if let resultJson = results.result.value as? [Dictionary<String, Any>]{
                for movieItem in resultJson{
                    let movie = Movie.init(fromDictionary: movieItem)
                    movies.append(movie)
                }
            }
            success(movies)
        }
    }
    func getMovie(urlType:String,userId:Int,onSuccess success: @escaping (_ movies: [Movie]) -> Void){
        let url = BASE_URL + "\(urlType)/\(userId)"
        print("movieList",url)

        Alamofire.request(url).responseJSON { (results) in
            var movies = [Movie]()
            if let resultJson = results.result.value as? [Dictionary<String, Any>]{
                for movieItem in resultJson{
                    let movie = Movie.init(fromDictionary: movieItem)
                    movies.append(movie)
                }
            }
            success(movies)
        }
    }
    func getListPopularMovie(onSuccess success: @escaping (_ popularMovies: [PopularMovie]) -> Void){
        Alamofire.request(BASE_URL + "get_list_title_top_moview").responseJSON { (results) in
            var populars = [PopularMovie]()
            if let resultJson = results.result.value as? [Dictionary<String, Any>]{
                for popularItem in resultJson{
                    let popular = PopularMovie.init(fromDictionary: popularItem)
                    populars.append(popular)
                }
            }
            success(populars)
        }
    }
}


extension MovieApi{
    func doRateMovie(user_id:Int,movie_id:Int,rating:Int,onSuccess success: @escaping (_ result: Result) -> Void){
        let parameters:[String:Any] = [
            "user_id" : user_id,
            "movie_id" : movie_id,
            "rating" : rating
        ]
        let todoEndpoint: String = BASE_URL + "rate_movie"
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        Alamofire.request(todoEndpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            if let res = response.result.value as? [String:Any]{
                let result = Result.init(fromDictionary: res)
                success(result)
            }
        }
    }
}
