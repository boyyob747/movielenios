//
//  Config.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import Foundation
class UrlApi {
    class var BASE_URL:String {
        return "http://127.0.0.1:3000/get_top_ratings"
    }
    class var LOGIN:String {
        return UrlApi.BASE_URL + "/login"
    }
    class var REGISTER:String {
        return UrlApi.BASE_URL + "/register"
    }
    class var GET_TOP_RATINGS:String {
        return UrlApi.BASE_URL + "/get_top_ratings"
    }
}
