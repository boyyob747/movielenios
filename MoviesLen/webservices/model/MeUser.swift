//
//  MeUser.swift
//  MoviesLen
//
//  Created by Boy- on 13/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import Foundation
import RealmSwift
final class MeUser: Object {
    @objc dynamic var user_id = ""
    @objc dynamic var username = ""
}
