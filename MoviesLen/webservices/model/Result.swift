//
//  Result.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import Foundation

class Result:NSObject{
    var message:String!
    var success:Bool = false
    init(fromDictionary dictionary: [String:Any]){
        message = dictionary["message"] as? String ?? ""
        success = dictionary["success"] as? Bool ?? false
    }
    func toString() -> String{
        return "message => \(String(describing: message))\nsuccess = \(success)"
    }
}
