//
//  Movie.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on November 7, 2018

import Foundation


class Movie : NSObject{
    
    var action : Int!
    var adventure : Int!
    var animation : Int!
    var childrens : Int!
    var comedy : Int!
    var crime : Int!
    var documentary : Int!
    var drama : Int!
    var fantasy : Int!
    var filmNoir : Int!
    var horror : Int!
    var iMDbUrl : String!
    var movieId : Int!
    var musical : Int!
    var mystery : Int!
    var releaseDate : String!
    var romance : Int!
    var sciFi : Int!
    var thriller : Int!
    var title : String!
    var unknown : Int!
    var war : Int!
    var western : Int!
    var actors : String!
    var director : String!
    var genre : String!
    var imdbRating : String!
    var imdbVotes : String!
    var poster : String!
    var my_rate: Int!
    init(fromDictionary dictionary: [String:Any]){
        action = dictionary["action"] as? Int ?? 0
        adventure = dictionary["adventure"] as? Int ?? 0
        animation = dictionary["animation"] as? Int ?? 0
        childrens = dictionary["childrens"] as? Int ?? 0
        comedy = dictionary["comedy"] as? Int ?? 0
        crime = dictionary["crime"] as? Int ?? 0
        documentary = dictionary["documentary"] as? Int ?? 0
        drama = dictionary["drama"] as? Int ?? 0
        fantasy = dictionary["fantasy"] as? Int ?? 0
        filmNoir = dictionary["filmNoir"] as? Int ?? 0
        horror = dictionary["horror"] as? Int ?? 0
        iMDbUrl = dictionary["iMDbUrl"] as? String ?? ""
        movieId = dictionary["movie_id"] as? Int ?? 0
        musical = dictionary["musical"] as? Int ?? 0
        mystery = dictionary["mystery"] as? Int ?? 0
        releaseDate = dictionary["releaseDate"] as? String ?? ""
        romance = dictionary["romance"] as? Int ?? 0
        sciFi = dictionary["sciFi"] as? Int ?? 0
        thriller = dictionary["thriller"] as? Int ?? 0
        title = dictionary["title"] as? String ?? ""
        unknown = dictionary["unknown"] as? Int ?? 0
        war = dictionary["war"] as? Int ?? 0
        western = dictionary["western"] as? Int ?? 0
        actors = dictionary["actors"] as? String ?? ""
        director = dictionary["director"] as? String ?? ""
        genre = dictionary["genre"] as? String ?? ""
        imdbRating = dictionary["imdbRating"] as? String ?? ""
        imdbVotes = dictionary["imdbVotes"] as? String ?? ""
        poster = dictionary["poster"] as? String ?? ""
        my_rate = dictionary["my_rate"] as? Int ?? 0
    }
}
