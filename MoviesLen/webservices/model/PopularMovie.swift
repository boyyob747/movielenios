//
//  PopularMovie.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on November 8, 2018

import Foundation


class PopularMovie : NSObject{
    
    var keyGet : String!
    var poster : String!
    var title : String!
    
    init(fromDictionary dictionary: [String:Any]){
        keyGet = dictionary["key_get"] as? String ?? ""
        poster = dictionary["poster"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
    }
}
