//
//  Rating.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on November 8, 2018

import Foundation


class Rating : NSObject{
    
    var create : String!
    var movieId : Int!
    var rating : Int!
    var userId : Int!
    
    init(fromDictionary dictionary: [String:Any]){
        create = dictionary["create"] as? String ?? ""
        movieId = dictionary["movie_id"] as? Int ?? 0
        rating = dictionary["rating"] as? Int ?? 0
        userId = dictionary["user_id"] as? Int ?? 0
    }
}
