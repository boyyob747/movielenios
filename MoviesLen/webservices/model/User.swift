//
//  User.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on November 8, 2018

import Foundation
import RealmSwift

class User : NSObject{
    
    var age : Int!
    var gender : String!
    var occupation : String!
    var password : String!
    var userId : Int!
    var username : String!
    var zipCode : String!
    
    init(fromDictionary dictionary: [String:Any]){
        age = dictionary["age"] as? Int ?? 0
        gender = dictionary["gender"] as? String ?? ""
        occupation = dictionary["occupation"] as? String ?? ""
        password = dictionary["password"] as? String ?? ""
        userId = dictionary["user_id"] as? Int ?? 0
        username = dictionary["username"] as? String ?? ""
        zipCode = dictionary["zipCode"] as? String ?? ""
    }
  
}


class MeUser: Object {
    @objc dynamic var user_id = 0
    @objc dynamic var username = ""
}

func getUser() -> MeUser?{
    let realm = try! Realm()
    let user = realm.objects(MeUser.self).first
    return user
}
