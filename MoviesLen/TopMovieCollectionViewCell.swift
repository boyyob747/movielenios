//
//  TopMovieCollectionViewCell.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit

class TopMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMovie: UIImageView!
    
    @IBOutlet weak var title: UILabel!
}
