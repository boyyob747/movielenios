//
//  ListMovieViewController.swift
//  MoviesLen
//
//  Created by Boy- on 13/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit
import GravitySliderFlowLayout
import AlamofireImage
class ListMovieViewController: UIViewController {
    @IBOutlet weak var priceButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var productSubtitleLabel: UILabel!
    @IBOutlet weak var productTitleLabel: UILabel!
    
    @IBOutlet weak var titleList: UILabel!
    let images = [UIImage.init(named: "liked"), UIImage.init(named: "liked"), UIImage.init(named: "liked")]
    let titles = ["Beats Solo", "Beats X - White", "Beats Studio"]
    let subtitles = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "For most people, buying a new computer\ndoes not have to be as stressful as\n        buying a new car.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."]
    let prices = ["$30.00", "$19.00", "$60.00"]
    
    let collectionViewCellHeightCoefficient: CGFloat = 0.85
    let collectionViewCellWidthCoefficient: CGFloat = 0.55
    let priceButtonCornerRadius: CGFloat = 10
    let gradientFirstColor = UIColor(hex: "ff8181").cgColor
    let gradientSecondColor = UIColor(hex: "a81382").cgColor
    let cellsShadowColor = UIColor(hex: "2a002a").cgColor
    let productCellIdentifier = "ProductCollectionViewCell"
    
    private var itemsNumber = 1000
    
    var popularMovie:PopularMovie!
    var movies = [Movie]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if popularMovie == nil && self.movies != nil{
            self.titleList.text = "Movie list recommend for you"
            if self.movies.count > 0{
                self.productTitleLabel.text = self.movies[0].title
                self.productSubtitleLabel.text = self.movies[0].genre
                self.priceButton.setTitle("Rate", for: .normal)
            }
            self.configureCollectionView()
            self.configurePriceButton()
        }
        else{
            self.titleList.text = popularMovie.title
            if let userId = getUser()?.user_id{
                MovieApi().getMovie(urlType: popularMovie.keyGet, userId: userId) { (movieList) in
                    self.movies = movieList
                    if self.movies.count > 0{
                        self.productTitleLabel.text = self.movies[0].title
                        self.productSubtitleLabel.text = self.movies[0].genre
                        self.priceButton.setTitle("Rate", for: .normal)
                    }
                    self.configureCollectionView()
                    self.configurePriceButton()
                }
            }
        }
        
    }
    
    private func configureCollectionView() {
        let gravitySliderLayout = GravitySliderFlowLayout(with: CGSize(width: collectionView.frame.size.height * collectionViewCellWidthCoefficient, height: collectionView.frame.size.height * collectionViewCellHeightCoefficient))
        collectionView.collectionViewLayout = gravitySliderLayout
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func configurePriceButton() {
        priceButton.layer.cornerRadius = priceButtonCornerRadius
    }
    
    private func configureProductCell(_ cell: ProductCollectionViewCell, for indexPath: IndexPath) {
        cell.clipsToBounds = false
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = cell.bounds
        gradientLayer.colors = [gradientFirstColor, gradientSecondColor]
        gradientLayer.cornerRadius = 21
        gradientLayer.masksToBounds = true
        cell.layer.insertSublayer(gradientLayer, at: 0)
        
        cell.layer.shadowColor = cellsShadowColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowRadius = 20
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 30)
        let index = indexPath.row % self.movies.count
        if index < self.movies.count{
            if let urlImg = URL.init(string: self.movies[index].poster){
                cell.productImage.af_setImage(withURL: urlImg)
                if self.movies[index].my_rate > 0 {
                    cell.newLabel.text = "" + String(self.movies[index].my_rate)
                    cell.newLabel.textColor = UIColor.white
                }else{
                    cell.newLabel.text = "New"
                    cell.newLabel.textColor = UIColor.white
                }
            }
        }
        
        cell.newLabel.layer.cornerRadius = 8
        cell.newLabel.clipsToBounds = true
        cell.newLabel.layer.borderColor = UIColor.white.cgColor
        cell.newLabel.layer.borderWidth = 1.0
    }
    
    private func animateChangingTitle(for indexPath: IndexPath) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        if let index = visibleIndexPath?.row{
            if index < self.movies.count{
                if let userId = getUser()?.user_id{
                        UIView.transition(with: self.productTitleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
                                self.productTitleLabel.text = self.movies[index].title
                        }, completion: nil)
                        UIView.transition(with: self.productSubtitleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
                                self.productSubtitleLabel.text = self.movies[index].genre
                        }, completion: nil)
                        UIView.transition(with: self.priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                                self.priceButton.setTitle("Rate", for: .normal)
                        }, completion: nil)
                    }
            }
        }
        
    }
    
    @IBAction func didPressPriceButton(_ sender: Any) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        if let index = visibleIndexPath?.row{
            if index < self.movies.count{
                if let userId = getUser()?.user_id{
                    MovieApi().doRateMovie(user_id: userId, movie_id: self.movies[index].movieId, rating: 1) { (result) in
                        self.movies[index].my_rate = 1
                        self.collectionView.reloadData()
                    }
                }
                
            }
        }
    }
    
}

extension ListMovieViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellIdentifier, for: indexPath) as! ProductCollectionViewCell
        self.configureProductCell(cell, for: indexPath)
        return cell
    }
    
    
}

extension ListMovieViewController: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let locationFirst = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x, y: collectionView.center.y + scrollView.contentOffset.y)
        let locationSecond = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x + 20, y: collectionView.center.y + scrollView.contentOffset.y)
        let locationThird = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x - 20, y: collectionView.center.y + scrollView.contentOffset.y)
        
        if let indexPathFirst = collectionView.indexPathForItem(at: locationFirst), let indexPathSecond = collectionView.indexPathForItem(at: locationSecond), let indexPathThird = collectionView.indexPathForItem(at: locationThird), indexPathFirst.row == indexPathSecond.row && indexPathSecond.row == indexPathThird.row && indexPathFirst.row != pageControl.currentPage {
            pageControl.currentPage = indexPathFirst.row % self.movies.count
            self.animateChangingTitle(for: indexPathFirst)
        }
    }
}
