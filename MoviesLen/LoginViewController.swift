//
//  LoginViewController.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RealmSwift
class LoginViewController: UIViewController {

    @IBOutlet weak var txtFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFieldPassword: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
        self.navigationController?.isToolbarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    @IBAction func onClickLogin(_ sender: Any) {
        let username = txtFieldUsername.text
        let password = txtFieldPassword.text

        if username == ""{
            print("DebugLogin","Username is empty!")
            return
        }
        if password == ""{
            print("DebugLogin","Password is empty!")
            return
        }
        if username != "" && password != ""{
            MovieApi().login(username: username ?? "", password: password ?? "") { (result) in
                if result.success{
                    MovieApi().getUser(username: username ?? "" , onSuccess: { (user) in
                        if user != nil{
                            let realm = try! Realm()
                            let meUser = MeUser()
                            meUser.username = user?.username ?? ""
                            meUser.user_id = user?.userId ?? 1
                            try! realm.write {
                                realm.add(meUser)
                            }
                        }
                    })
                    
                    _ = self.navigationController?.popViewController(animated: false)
                    UserDefaults.standard.set(true, forKey: "isLogined")
                }else{
                    let refreshAlert = UIAlertController(title: "", message: result.message, preferredStyle: UIAlertController.Style.alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        refreshAlert.dismiss(animated: false, completion: {
                        })
                    }))
                    self.present(refreshAlert, animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func onClickRegister(_ sender: Any) {
        self.performSegue(withIdentifier: "RegisterSegue", sender: self)
    }
}
