//
//  RegisterViewController.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class RegisterViewController: UIViewController {

    @IBOutlet weak var txtFieldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFieldUsername: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickRegister(_ sender: Any) {
        guard let username = txtFieldUsername.text else {
            return
        }
        guard let password = txtFieldPassword.text else {
            return
        }
        if username == "" || password == ""{
            return
        }
        
        MovieApi().register(username: username, password: password) { (result) in
            print("DebugRegister",result.toString())
            if result.success{
                _ = self.navigationController?.popViewController(animated: false)
                UserDefaults.standard.set(true, forKey: "isLogined")
            }else{
                let refreshAlert = UIAlertController(title: "", message: result.message, preferredStyle: UIAlertController.Style.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    refreshAlert.dismiss(animated: false, completion: {
                        
                    })
                }))
                self.present(refreshAlert, animated: true, completion: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
