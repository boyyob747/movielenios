//
//  MovieDetilsViewController.swift
//  MoviesLen
//
//  Created by Boy- on 13/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit

class MovieDetilsViewController: UIViewController {

    var movie:Movie!
    @IBOutlet weak var imgTop: UIImageView!
    
    @IBOutlet weak var titleMovie: UILabel!
    
    @IBOutlet weak var imageMovie: UIImageView!
    
    @IBOutlet weak var desMovie: UILabel!
    
    @IBOutlet weak var btnRate: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if movie != nil{
            if let url = URL.init(string: movie.poster){
                imageMovie.af_setImage(withURL: url)
                imgTop.af_setImage(withURL: url)
            }
            titleMovie.text = movie.title
            desMovie.text = movie.actors + "\n" + movie.director
            self.title = movie.title
            print("poster",movie.movieId)
        }
    }

    @IBAction func onClickRate(_ sender: Any) {
        if let user = getUser(){
            MovieApi().doRateMovie(user_id: user.user_id, movie_id: self.movie.movieId, rating: 1) { (result) in
                print("result",user.user_id,self.movie.movieId)
                print("result",result.toString())
            }

        }
    }
}
