
//  ViewController.swift
//  MoviesLen
//
//  Created by Boy- on 8/11/2561 BE.
//  Copyright © 2561 Boy-. All rights reserved.
//

import UIKit
import AlamofireImage
import RealmSwift
class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cvRecommender: UICollectionView!
    var movie:Movie!
    var popularMovie:PopularMovie!
    @IBOutlet weak var cvTopMovies: UICollectionView!
    var popularMoviesList = [PopularMovie]()
    var moviesRecommend = [Movie]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        let me_user = realm.objects(MeUser.self).first
        if me_user == nil{
            self.performSegue(withIdentifier: "LoginSegue", sender: self)
        }
        cvTopMovies.delegate = self
        cvTopMovies.dataSource = self
        cvRecommender.delegate = self
        cvRecommender.dataSource = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        print("user",me_user)
        MovieApi().getListPopularMovie { (popularsResult) in
            self.popularMoviesList = popularsResult
            self.cvTopMovies.reloadData()
        }
        MovieApi().getRecommender(userId: me_user?.user_id ?? 1) { (moviesResult) in
            self.moviesRecommend = moviesResult
            self.cvRecommender.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
        self.navigationController?.isToolbarHidden = true
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "titlePopularListId", for: indexPath)
        return headerView
    }
    //movieListSegue
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == cvTopMovies ? self.popularMoviesList.count : self.moviesRecommend.count
    }
    
    @objc func tapList(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.cvTopMovies)
        let indexPath = self.cvTopMovies.indexPathForItem(at: location)
        
        if let index = indexPath?.row {
            if index < self.popularMoviesList.count{
                self.popularMovie = self.popularMoviesList[index]
                self.performSegue(withIdentifier: "movieListSegue", sender: self)
            }
        }
    }
    
    @objc func tapDetails(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.cvRecommender)
        let indexPath = self.cvRecommender.indexPathForItem(at: location)
        
        if let index = indexPath?.row {
            //            if index < self.moviesRecommend.count{
            self.movie = self.moviesRecommend[index]
            self.performSegue(withIdentifier: "movieDetailSegue", sender: self)
            //            }
        }
       
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "movieDetailSegue" {
            let vc = segue.destination as! MovieDetilsViewController
            vc.movie = self.movie
        }else if segue.identifier == "movieListSegue" {
            let vc = segue.destination as! ListMovieViewController
            if self.popularMovie != nil{
                vc.popularMovie = self.popularMovie
            }else{
                vc.movies = self.moviesRecommend
            }
        }
    }
    
    @IBAction func onViewAll(_ sender: Any) {
        self.popularMovie = nil
        self.performSegue(withIdentifier: "movieListSegue", sender: self)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvTopMovies {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopMovieIdentity", for: indexPath as IndexPath) as! TopMovieCollectionViewCell
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapList(_:))))
            cell.title.text = self.popularMoviesList[indexPath.row].title
            if let url = URL.init(string: self.popularMoviesList[indexPath.row].poster){
                cell.imgMovie.af_setImage(withURL: url)
            }else{
                cell.imgMovie.image = UIImage(named: "imdb_logo")
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieRecommendId", for: indexPath as IndexPath) as! MovieRecommenderCollectionViewCell
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapDetails(_:))))
            cell.lbTitle.text = self.moviesRecommend[indexPath.row].title
            if let url = URL.init(string: self.moviesRecommend[indexPath.row].poster){
                cell.imgMovie.af_setImage(withURL: url)
            }else{
                cell.imgMovie.image = UIImage(named: "imdb_logo")
            }
            return cell
        }
        
    }
}

